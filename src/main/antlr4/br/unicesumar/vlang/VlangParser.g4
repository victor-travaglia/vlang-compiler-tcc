parser grammar VlangParser;

options { tokenVocab = VlangLexer; }

program
    : DECLARE declaration* ALGORITHM command* EOF
    ;

declaration
    : VARIABLE COLON VARIABLE_TYPE
    ;

arithmeticExpression
    : arithmeticTerm (ARITHMETIC_OPERATOR arithmeticTerm)*
    ;

arithmeticTerm
    : arithmeticFactor (ARITHMETIC_OPERATOR arithmeticFactor)*
    ;

arithmeticFactor
    : INTEGER_NUMBER
    | FLOAT_NUMBER
    | VARIABLE
    | LEFT_PAREN arithmeticExpression RIGHT_PAREN
    ;

relationalExpression
    : relationalTerm (LOGICAL_OPERATOR relationalTerm)*
    ;

relationalTerm
    : arithmeticExpression RELATIONAL_OPERATOR arithmeticExpression
    | LEFT_PAREN relationalExpression RIGHT_PAREN
    ;

command
    : assignmentCommand
    | inputCommand
    | outputCommand
    | conditionCommand
    | repeatCommand
    | subAlgorithm
    ;

assignmentCommand
    : ASSIGN arithmeticExpression TO VARIABLE
    ;

inputCommand
    : READ VARIABLE
    ;

outputCommand
    : WRITE (arithmeticExpression
    | STRING)
    ;

conditionCommand
    : IF relationalExpression THEN command (ELSE command)?
    ;

repeatCommand
    : WHILE relationalExpression command
    ;

subAlgorithm
    : BEGIN command* END
    ;