lexer grammar VlangLexer;

VARIABLE_TYPE
    : 'INTEGER' | 'FLOAT'
    ;

INTEGER_NUMBER
    : ('0'..'9')+
    ;

FLOAT_NUMBER
    : ('0'..'9')+ '.' ('0'..'9')+
    ;

STRING
    : '\'' ( ESCAPE_SEQUENCE | ~('\''|'\\') )* '\''
    ;

DECLARE
    : 'DECLARE'
    ;

ALGORITHM
    : 'ALGORITHM'
    ;

COLON
    : ':'
    ;

LEFT_PAREN
    : '('
    ;

RIGHT_PAREN
    : ')'
    ;

ASSIGN
    : 'ASSIGN'
    ;

TO
    : 'TO'
    ;

READ
    : 'READ'
    ;

WRITE
    : 'WRITE'
    ;

IF
    : 'IF'
    ;

THEN
    : 'THEN'
    ;

ELSE
    : 'ELSE'
    ;

WHILE
    : 'WHILE'
    ;

BEGIN
    : 'BEGIN'
    ;

END
    : 'END'
    ;

fragment
ESCAPE_SEQUENCE
    : '\\\''
    ;

ARITHMETIC_OPERATOR
    : '+'
    | '-'
    | '*'
    | '/'
    ;

RELATIONAL_OPERATOR
    : '>'
    | '>='
    | '<'
    | '<='
    | '<>'
    | '='
    ;

LOGICAL_OPERATOR
    : 'AND'
    | 'OR'
    ;

VARIABLE
    : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')*
    ;

COMMENT
    : '%' ~('\n'|'\r')* '\r'? '\n' -> skip
    ;

WS
    : ( ' ' |'\t' | '\r' | '\n') -> skip
    ;
