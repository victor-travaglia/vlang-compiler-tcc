package br.unicesumar.vlang.parser;

import br.unicesumar.vlang.VlangParser;
import br.unicesumar.vlang.VlangParserBaseVisitor;
import br.unicesumar.vlang.parser.model.SymbolTable;
import br.unicesumar.vlang.parser.model.VLangType;
import br.unicesumar.vlang.utils.SemanticUtils;

import static br.unicesumar.vlang.utils.Constants.*;

public class VLangParserVisitor extends VlangParserBaseVisitor<Void> {

    // https://github.com/dlucredio/cursocompiladores/blob/master/exemplosCodigo/Compiladores.T%C3%B3pico07.An%C3%A1lise%20Sem%C3%A2ntica.roteiro.md

    private SymbolTable table;

    @Override
    public Void visitProgram(VlangParser.ProgramContext ctx) {
        table = new SymbolTable();

        return super.visitProgram(ctx);
    }

    @Override
    public Void visitDeclaration(VlangParser.DeclarationContext ctx) {
        String variableName = ctx.VARIABLE().getText();
        String variableType = ctx.VARIABLE_TYPE().getText();

        VLangType type;

        switch (variableType) {
            case "INTEGER":
                type = VLangType.INTEGER;
                break;
            case "FLOAT":
                type = VLangType.FLOAT;
                break;
            default:
                type = VLangType.INVALID;
                break;
        }

        if (table.exists(variableName)) {
            SemanticUtils.addSemanticError(ctx.VARIABLE().getSymbol(), String.format(VARIABLE_ALREADY_EXISTS_ERROR, variableName));
        } else {
            table.add(variableName, type);
        }

        return super.visitDeclaration(ctx);
    }

    @Override
    public Void visitAssignmentCommand(VlangParser.AssignmentCommandContext ctx) {
        VLangType type = SemanticUtils.verifyType(table, ctx.arithmeticExpression());

        if (!type.equals(VLangType.INVALID)) {
            String variableName = ctx.VARIABLE().getText();

            if (!table.exists(variableName)) {
                SemanticUtils.addSemanticError(ctx.VARIABLE().getSymbol(), String.format(VARIABLE_NOT_DECLARE_ERROR, variableName));
            }

            if (!SemanticUtils.verifyType(table, variableName).equals(type)) {
                SemanticUtils.addSemanticError(ctx.VARIABLE().getSymbol(), String.format(VARIABLE_TYPE_COMPATIBILITY_ERROR, variableName));
            }
        }

        return super.visitAssignmentCommand(ctx);
    }

    @Override
    public Void visitInputCommand(VlangParser.InputCommandContext ctx) {
        String variableName = ctx.VARIABLE().getText();

        if (!table.exists(variableName)) {
            SemanticUtils.addSemanticError(ctx.VARIABLE().getSymbol(), VARIABLE_NOT_DECLARE_ERROR);
        }

        return super.visitInputCommand(ctx);
    }

    @Override
    public Void visitArithmeticExpression(VlangParser.ArithmeticExpressionContext ctx) {
        SemanticUtils.verifyType(table, ctx);

        return super.visitArithmeticExpression(ctx);
    }

}
