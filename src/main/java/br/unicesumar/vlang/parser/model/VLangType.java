package br.unicesumar.vlang.parser.model;

public enum VLangType {
    INTEGER,
    FLOAT,
    INVALID;
}
