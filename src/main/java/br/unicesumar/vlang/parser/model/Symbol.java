package br.unicesumar.vlang.parser.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Symbol {

    private String name;
    private VLangType type;
}
