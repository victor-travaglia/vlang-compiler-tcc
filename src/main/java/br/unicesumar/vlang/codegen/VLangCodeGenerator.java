package br.unicesumar.vlang.codegen;

import br.unicesumar.vlang.VlangParser;
import br.unicesumar.vlang.VlangParserBaseVisitor;
import br.unicesumar.vlang.parser.model.SymbolTable;
import br.unicesumar.vlang.parser.model.VLangType;
import br.unicesumar.vlang.utils.SemanticUtils;
import lombok.Getter;

import java.util.stream.IntStream;

import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.SPACE;

public class VLangCodeGenerator extends VlangParserBaseVisitor<Void> {

    @Getter
    private final StringBuilder output;
    private final SymbolTable table;

    public VLangCodeGenerator() {
        this.output = new StringBuilder();
        this.table = new SymbolTable();
    }

    @Override
    public Void visitProgram(VlangParser.ProgramContext ctx) {
        output.append(format("package br.unicesumar.vlang.runner.output; %s", lineSeparator()));
        output.append(format("import java.util.Scanner; %s", lineSeparator()));
        output.append(format("public class VLangCompiledClass { %s", lineSeparator()));
        output.append(lineSeparator());
        output.append(format("public static void main(String[] args) { %s", lineSeparator()));
        output.append(format("Scanner scanner = new Scanner(System.in); %s", lineSeparator()));

        ctx.declaration().forEach(this::visitDeclaration);

        output.append(lineSeparator());

        ctx.command().forEach(this::visitCommand);

        output.append(lineSeparator());
        output.append(format("} %s", lineSeparator()));
        output.append(format("} %s", lineSeparator()));

        return null;
    }

    @Override
    public Void visitDeclaration(VlangParser.DeclarationContext ctx) {
        String variableName = ctx.VARIABLE().getText();
        String variableType = ctx.VARIABLE_TYPE().getText();
        VLangType type = VLangType.INVALID;

        switch (variableType) {
            case "INTEGER":
                type = VLangType.INTEGER;
                variableType = format("%s", "Integer");
                break;
            case "FLOAT":
                type = VLangType.FLOAT;
                variableType = format("%s", "Double");
                break;
            default:
                break;
        }

        table.add(variableName, type);

        if (type.equals(VLangType.INTEGER)) {
            output.append(format("%s%s%s = 0; %s", variableType, SPACE, variableName, lineSeparator()));
        }

        if (type.equals(VLangType.FLOAT)) {
            output.append(format("%s%s%s = 0.0; %s", variableType, SPACE, variableName, lineSeparator()));
        }

        return null;
    }

    @Override
    public Void visitArithmeticExpression(VlangParser.ArithmeticExpressionContext ctx) {
        visitArithmeticTerm(ctx.arithmeticTerm(0));

        IntStream.range(0, ctx.ARITHMETIC_OPERATOR().size()).forEach(index -> {
            output.append(format("%s%s%s", SPACE, ctx.ARITHMETIC_OPERATOR(index), SPACE));
            visitArithmeticTerm(ctx.arithmeticTerm(index + 1));
        });

        return null;
    }

    @Override
    public Void visitArithmeticTerm(VlangParser.ArithmeticTermContext ctx) {
        visitArithmeticFactor(ctx.arithmeticFactor(0));

        IntStream.range(0, ctx.ARITHMETIC_OPERATOR().size()).forEach(index -> {
            output.append(format("%s%s%s", SPACE, ctx.ARITHMETIC_OPERATOR(index), SPACE));
            visitArithmeticFactor(ctx.arithmeticFactor(index + 1));
        });

        return null;
    }

    @Override
    public Void visitArithmeticFactor(VlangParser.ArithmeticFactorContext ctx) {
        if (nonNull(ctx.INTEGER_NUMBER())) {
            output.append(format("%s", ctx.INTEGER_NUMBER().getText()));
        } else if (nonNull(ctx.FLOAT_NUMBER())) {
            output.append(format("%s", ctx.FLOAT_NUMBER().getText()));
        } else if (nonNull(ctx.VARIABLE())) {
            output.append(format("%s", ctx.VARIABLE().getText()));
        } else {
            output.append(format("%s", "("));

            visitArithmeticExpression(ctx.arithmeticExpression());

            output.append(format("%s", ")"));
        }

        return null;
    }

    @Override
    public Void visitRelationalExpression(VlangParser.RelationalExpressionContext ctx) {
        visitRelationalTerm(ctx.relationalTerm(0));

        IntStream.range(0, ctx.LOGICAL_OPERATOR().size()).forEach(index -> {
            String actualTerm;

            if (ctx.LOGICAL_OPERATOR(index).getText().equals("AND")) {
                actualTerm = format("%s", "&&");
            } else {
                actualTerm = format("%s", "||");
            }
            output.append(format("%s%s%S", SPACE, actualTerm, SPACE));

            visitRelationalTerm(ctx.relationalTerm(index + 1));
        });

        return null;
    }

    @Override
    public Void visitRelationalTerm(VlangParser.RelationalTermContext ctx) {
        if (nonNull(ctx.relationalExpression())) {
            output.append(format("%s", "("));

            visitRelationalExpression(ctx.relationalExpression());

            output.append(format("%s", ")"));
        } else {
            visitArithmeticExpression(ctx.arithmeticExpression(0));

            String actualOperator = ctx.RELATIONAL_OPERATOR().getText();

            if (actualOperator.equals("<>")) {
                actualOperator = format("%s", "!=");
            } else if (actualOperator.equals("=")) {
                actualOperator = format("%s", "==");
            }
            output.append(format("%s%s%s", SPACE, actualOperator, SPACE));

            visitArithmeticExpression(ctx.arithmeticExpression(1));
        }

        return null;
    }

    @Override
    public Void visitCommand(VlangParser.CommandContext ctx) {
        return super.visitCommand(ctx);
    }

    @Override
    public Void visitAssignmentCommand(VlangParser.AssignmentCommandContext ctx) {
        output.append(format("%s%s", ctx.VARIABLE().getText(), " = "));

        visitArithmeticExpression(ctx.arithmeticExpression());

        output.append(format("%s %s", ";", lineSeparator()));

        return null;
    }

    @Override
    public Void visitInputCommand(VlangParser.InputCommandContext ctx) {
        String variableName = ctx.VARIABLE().getText();
        VLangType type = SemanticUtils.verifyType(table, variableName);
        String actualVar = EMPTY;

        switch (type) {
            case INTEGER:
                actualVar = format("scanner.nextInt()%s %s", ";", lineSeparator());
                break;
            case FLOAT:
                actualVar = format("scanner.nextDouble()%s %s", ";", lineSeparator());
        }

        output.append(format("%s %s %s %s", variableName, "=", actualVar, lineSeparator()));

        return null;
    }

    @Override
    public Void visitOutputCommand(VlangParser.OutputCommandContext ctx) {
        if (nonNull(ctx.STRING())) {
            String actualString = ctx.STRING().getText();

            output.append(format("System.out.println(\"%s\")%s",
                    actualString.substring(1, actualString.length() - 1), ";"));
        } else {
            output.append(format("System.out.println%s", "("));

            visitArithmeticExpression(ctx.arithmeticExpression());

            output.append(format("%s%s %s", ")", ";", lineSeparator()));
        }

        return null;
    }

    @Override
    public Void visitConditionCommand(VlangParser.ConditionCommandContext ctx) {
        output.append(format("%s %s", "if", "("));

        visitRelationalExpression(ctx.relationalExpression());

        output.append(format("%s %s", ")", lineSeparator()));

        visitCommand(ctx.command(0));

        if (ctx.command().size() > 1) {
            output.append(format("%s %s %s", "else", "{", lineSeparator()));
            visitCommand(ctx.command(1));
            output.append(format("%s %s", "}", lineSeparator()));
        }

        return null;
    }

    @Override
    public Void visitRepeatCommand(VlangParser.RepeatCommandContext ctx) {
        output.append(format("%s %s", "while", "("));

        visitRelationalExpression(ctx.relationalExpression());

        output.append(format("%s %S", ")", lineSeparator()));

        visitCommand(ctx.command());

        return null;
    }

    @Override
    public Void visitSubAlgorithm(VlangParser.SubAlgorithmContext ctx) {
        output.append(format("%s %s", "{", lineSeparator()));

        ctx.command().forEach(this::visitCommand);

        output.append(format("%s %s", "}", lineSeparator()));

        return null;
    }
}
