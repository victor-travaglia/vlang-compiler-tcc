package br.unicesumar.vlang;

import br.unicesumar.vlang.codegen.VLangCodeGenerator;
import br.unicesumar.vlang.listener.SyntaxErrorListener;
import br.unicesumar.vlang.parser.VLangParserVisitor;
import br.unicesumar.vlang.runner.CompiledClassRunner;
import br.unicesumar.vlang.utils.Constants;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.PrintWriter;

import static br.unicesumar.vlang.utils.Errors.getErrors;

public class VLangCompiler {

    public static void main(String[] args) {
        try {
            CharStream cs = CharStreams.fromFileName(args[0]);
            VlangLexer lexer = new VlangLexer(cs);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            VlangParser parser = new VlangParser(tokens);
            SyntaxErrorListener syntaxError = new SyntaxErrorListener();

            parser.removeErrorListeners();
            parser.addErrorListener(syntaxError);

            VlangParser.ProgramContext program = parser.program();

            VLangParserVisitor semantics = new VLangParserVisitor();
            semantics.visitProgram(program);

            if (getErrors().isEmpty()) {
                VLangCodeGenerator vLangCodeGenerator = new VLangCodeGenerator();

                vLangCodeGenerator.visitProgram(program);

                try(PrintWriter printWriter = new PrintWriter(Constants.COMPILED_CLASS_PATH)) {
                    printWriter.print(vLangCodeGenerator.getOutput().toString());
                }
            }
        }
        catch (Exception ignored) {
        }
        finally {
            if (getErrors().isEmpty()) {
                CompiledClassRunner.run();
            } else {
                getErrors().forEach(System.err::println);
            }
        }
    }
}
