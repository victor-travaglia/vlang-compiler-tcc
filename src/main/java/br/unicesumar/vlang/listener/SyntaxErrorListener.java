package br.unicesumar.vlang.listener;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import static br.unicesumar.vlang.utils.Errors.addError;
import static java.lang.String.format;

public class SyntaxErrorListener extends BaseErrorListener {

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer,
                            Object offendingSymbol,
                            int line,
                            int charPositionInLine,
                            String msg,
                            RecognitionException e) {
        addError(format("Syntax Error %d:%d - %s", line, charPositionInLine + 1, msg));
    }
}
