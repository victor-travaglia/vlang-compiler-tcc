package br.unicesumar.vlang.runner;

import br.unicesumar.vlang.runner.output.VLangCompiledClass;

public final class CompiledClassRunner {

    public static void run() {
        VLangCompiledClass.main(null);
    }
}
