package br.unicesumar.vlang.utils;

public final class Constants {

    public static final String COMPILED_CLASS_PATH = "src/main/java/br/unicesumar/vlang/runner/output/VLangCompiledClass.java";
    public static final String VARIABLE_NOT_DECLARE_ERROR = "variable %s was not declared before use";
    public static final String VARIABLE_TYPE_COMPATIBILITY_ERROR = "variable type %s is not compatible with expression type";
    public static final String VARIABLE_ALREADY_EXISTS_ERROR = "variable type %s is not compatible with expression type";

}
