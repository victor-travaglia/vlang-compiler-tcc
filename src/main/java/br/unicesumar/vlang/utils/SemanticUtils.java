package br.unicesumar.vlang.utils;

import br.unicesumar.vlang.VlangParser;
import br.unicesumar.vlang.parser.model.SymbolTable;
import br.unicesumar.vlang.parser.model.VLangType;
import org.antlr.v4.runtime.Token;

import java.util.concurrent.atomic.AtomicReference;

import static br.unicesumar.vlang.utils.Errors.*;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public final class SemanticUtils {

    public static void addSemanticError(Token token, String message) {
        addError(String.format("Semantic Error %d:%d - %s", token.getLine(), token.getCharPositionInLine(), message));
    }

    public static VLangType verifyType(SymbolTable table, VlangParser.ArithmeticExpressionContext ctx) {
        AtomicReference<VLangType> type = new AtomicReference<>();

        ctx.arithmeticTerm().forEach(arithmeticTermContext -> {
            VLangType actualType = verifyType(table, arithmeticTermContext);

            if (isNull(type.get())) {
                type.set(actualType);
            } else if (type.get() != actualType && actualType != VLangType.INVALID) {
                addError(ctx.start.getType() + " expression" + ctx.getText() + " contains incompatible types.");
                type.set(VLangType.INVALID);
            }
        });

        return type.get();
    }

    private static VLangType verifyType(SymbolTable table, VlangParser.ArithmeticTermContext ctx) {
        AtomicReference<VLangType> type = new AtomicReference<>();

        ctx.arithmeticFactor().forEach(arithmeticFactorContext -> {
            VLangType actualType = verifyType(table, arithmeticFactorContext);

            if (isNull(type.get())) {
                type.set(actualType);
            } else if (type.get() != actualType && actualType != VLangType.INVALID) {
                addError(ctx.start.getType() + " term" + ctx.getText() + " contains incompatible types.");
                type.set(VLangType.INVALID);
            }
        });

        return type.get();
    }

    private static VLangType verifyType(SymbolTable table, VlangParser.ArithmeticFactorContext ctx) {
        if (nonNull(ctx.INTEGER_NUMBER())) {
            return VLangType.INTEGER;
        }

        if (nonNull(ctx.FLOAT_NUMBER())) {
            return VLangType.FLOAT;
        }

        if (nonNull(ctx.VARIABLE())) {
            String variableName = ctx.VARIABLE().getText();

            if (table.exists(variableName)) {
                return verifyType(table, variableName);
            }

            addSemanticError(ctx.VARIABLE().getSymbol(), "variable " + variableName + " was not declared before use.");
        }

        return verifyType(table, ctx.arithmeticExpression());
    }

    public static VLangType verifyType(SymbolTable table, String variableName) {
        return table.verifyType(variableName);
    }

}
