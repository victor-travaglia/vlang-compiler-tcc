package br.unicesumar.vlang.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Errors {

    private static final List<String> errors = new ArrayList<>();

    public static void addError(String error) {
        errors.add(error);
    }

    public static List<String> getErrors() {
        return Collections.unmodifiableList(errors);
    }
}
