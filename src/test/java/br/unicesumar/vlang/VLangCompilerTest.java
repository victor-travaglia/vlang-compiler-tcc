package br.unicesumar.vlang;

import org.junit.jupiter.api.Test;

class VLangCompilerTest {

    @Test
    public void shouldPass() {
        String[] param = new String[2];
        param[0] = "src/test/resources/teste";
        VLangCompiler.main(param);
    }
}